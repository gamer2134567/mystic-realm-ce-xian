local portalopener = 0
local pillar1 = false
local pillar2 = false
local pillar3 = false
local pillar4 = false
local pillar5 = false
local pillar6 = false
local pillar7 = false

addHook("NetVars", function(net)
	portalopener = net($)
	pillar1 = net($)
	pillar2 = net($)
	pillar3 = net($)
	pillar4 = net($)
	pillar5 = net($)
	pillar6 = net($)
	pillar7 = net($)
end)

local function secretexit(line, mo, d)
	if mrce_hyperunlocked == true
		P_LinedefExecute(4000, nil)
	end
end

addHook("LinedefExecute", secretexit, "SECTEXIT")

local function emerald1(line, mo, d)
	if ((emeralds & EMERALD1) == 1)
		local em1 = P_SpawnMobj(0, -1344*FRACUNIT, 184*FRACUNIT, MT_FAKEEMERALD1)
		portalopener = $ + 1
		pillar1 = true
		S_StartSoundAtVolume(em1, sfx_s3k9c, 150)
	end
end

addHook("LinedefExecute", emerald1, "SEMMY1")

local function emerald2(line, mo, d)
	if ((emeralds & EMERALD2) == 2)
		--print("yes")
		local em2 = P_SpawnMobj(1064*FRACUNIT, -816*FRACUNIT, 184*FRACUNIT, MT_FAKEEMERALD2)
		portalopener = $ + 1
		pillar2 = true
		S_StartSoundAtVolume(em2, sfx_s3k9c, 150)
	else
		--print("nah")
	end
end

addHook("LinedefExecute", emerald2, "SEMMY2")

local function emerald3(line, mo, d)
	if ((emeralds & EMERALD3) == 4)
		--print("yes")
		local em3 = P_SpawnMobj(1304*FRACUNIT, 320*FRACUNIT, 184*FRACUNIT, MT_FAKEEMERALD3)
		portalopener = $ + 1
		pillar3 = true
		S_StartSoundAtVolume(em3, sfx_s3k9c, 150)
	else
		--print("nah")
	end
end

addHook("LinedefExecute", emerald3, "SEMMY3")

local function emerald4(line, mo, d)
	if ((emeralds & EMERALD4) == 8)
		--print("yes")
		local em4 = P_SpawnMobj(568*FRACUNIT, 1216*FRACUNIT, 184*FRACUNIT, MT_FAKEEMERALD4)
		portalopener = $ + 1
		pillar4 = true
		S_StartSoundAtVolume(em4, sfx_s3k9c, 150)
	else
		--print("nah")
	end
end

addHook("LinedefExecute", emerald4, "SEMMY4")

local function emerald5(line, mo, d)
	if ((emeralds & EMERALD5) == 16)
		--print("yes")
		local em5 = P_SpawnMobj(-568*FRACUNIT, 1216*FRACUNIT, 184*FRACUNIT, MT_FAKEEMERALD5)
		portalopener = $ + 1
		pillar5 = true
		S_StartSoundAtVolume(em5, sfx_s3k9c, 150)
	else
		--print("nah")
	end
end

addHook("LinedefExecute", emerald5, "SEMMY5")

local function emerald6(line, mo, d)
	if ((emeralds & EMERALD6) == 32)
		--print("yes")
		local em6 = P_SpawnMobj(-1304*FRACUNIT, 320*FRACUNIT, 184*FRACUNIT, MT_FAKEEMERALD6)
		portalopener = $ + 1
		pillar6 = true
		S_StartSoundAtVolume(em6, sfx_s3k9c, 150)
	else
		--print("nah")
	end
end

addHook("LinedefExecute", emerald6, "SEMMY6")

local function emerald7(line, mo, d)
	if ((emeralds & EMERALD7) == 64)
		--print("yes")
		local em7 = P_SpawnMobj(-1064*FRACUNIT, -817*FRACUNIT, 184*FRACUNIT, MT_FAKEEMERALD7)
		portalopener = $ + 1
		pillar7 = true
		S_StartSoundAtVolume(em7, sfx_s3k9c, 150)
	else
		--print("nah")
	end
end

addHook("LinedefExecute", emerald7, "SEMMY7")

addHook("ThinkFrame", function()
	if gamemap == 97 then
		G_SetCustomExitVars(122,1)
		G_ExitLevel()
	end
	for p in players.iterate
		if pillar1 == true then
			P_LinedefExecute(6001, p.mo, 6000)
			pillar1 = false
		end
		if pillar2 == true then
			P_LinedefExecute(6002, p.mo, 6003)
			pillar2 = false
		end
		if pillar3 == true then
			P_LinedefExecute(6004, p.mo, 6005)
			pillar3 = false
		end
		if pillar4 == true then
			P_LinedefExecute(6006, p.mo, 6007)
			pillar4 = false
		end
		if pillar5 == true then
			P_LinedefExecute(6008, p.mo, 6009)
			pillar5 = false
		end
		if pillar6 == true then
			P_LinedefExecute(6010, p.mo, 6011)
			pillar6 = false
		end
		if pillar7 == true then
			P_LinedefExecute(6012, p.mo, 6013)
			pillar7 = false
		end
		if portalopener == 7 then
			S_StartSound(nil, sfx_s3k54)
			P_LinedefExecute(6014, p.mo)
			P_LinedefExecute(6015, p.mo)
			P_LinedefExecute(6016, p.mo)
			P_LinedefExecute(6017, p.mo)
			P_LinedefExecute(6018, p.mo)
			P_LinedefExecute(6019, p.mo)
			P_LinedefExecute(6020, p.mo)
			P_LinedefExecute(6021, p.mo)
			P_LinedefExecute(6022, p.mo)
			P_LinedefExecute(6023, p.mo)
			P_LinedefExecute(6024, p.mo)
			P_LinedefExecute(6025, p.mo)
			P_LinedefExecute(6026, p.mo)
			P_LinedefExecute(6027, p.mo)
			P_LinedefExecute(6028, p.mo)
			P_LinedefExecute(6029, p.mo)
			P_LinedefExecute(6030, p.mo)
			portalopener = 8
		end
	end
end)

addHook("MapLoad", function(p, v)
	if portalopener != 0 then
		portalopener = 0
		pillar1 = false
		pillar2 = false
		pillar3 = false
		pillar4 = false
		pillar5 = false
		pillar6 = false
		pillar7 = false
	end
end)

addHook("PlayerThink", function(player)
	if gamemap == 121 and player.exiting and portalopener == 8 and not netgame
		player.mo.flags2 = $|MF2_DONTDRAW
	end
end)