freeslot(
"MT_URING",
"MT_FAKEEMERALD1",
"MT_FAKEEMERALD2",
"MT_FAKEEMERALD3",
"MT_FAKEEMERALD4",
"MT_FAKEEMERALD5",
"MT_FAKEEMERALD6",
"MT_FAKEEMERALD7",
"MT_FBOSS",
"MT_XBOSS",
"MT_FBOSS2",
"MT_EGGANIMUS_EX",
"MT_EGGANIMUS"
)
sfxinfo[freeslot("sfx_marioe")].caption = "Correct Solution"
local debug = 0

//Ring Attraction function based upon P_Attract from source.
local function SuperAttract(source, dest)
	local dist = 0
	local ndist = 0
	local speedmul = 0
	local tx = dest.x
	local ty = dest.y
	local tz = dest.z + (dest.height/2)
	local xydist = P_AproxDistance(tx - source.x, ty - source.y)
	if dest and dest.health and dest.valid and dest.type == MT_PLAYER
		source.angle = R_PointToAngle2(source.x, source.y, tx, ty)
		dist = P_AproxDistance(xydist, tz - source.z)
		if (dist < 1)
			dist = 1
		end
		speedmul = P_AproxDistance(dest.momx, dest.momy) + FixedMul(source.info.speed, source.scale)
		source.momx = FixedMul(FixedDiv(tx - source.x, dist), speedmul)
		source.momy = FixedMul(FixedDiv(ty - source.y, dist), speedmul)
		source.momz = FixedMul(FixedDiv(tz - source.z, dist), speedmul)
		ndist = P_AproxDistance(P_AproxDistance(tx - (source.x + source.momx), ty - (source.y+source.momy)), tz - (source.z+source.momz))
		if (ndist > dist)
			source.momx = 0
			source.momy = 0
			source.momz = 0
			P_TeleportMove(source, tx, ty, tz)
		end
	end
end

addHook("MobjThinker", function(ring)
    if ring and ring.valid and ring.health > 0
        local soup
        for p in players.iterate
            if p and p.valid and ((p.mrce and (p.mrce.canhyper or p.mrce.ultrastar) and mrce_hyperunlocked and p.powers[pw_super]) or (mapheaderinfo[gamemap].lvlttl == "Dimension Warp"))  and R_PointToDist2(p.mo.x, p.mo.y, ring.x, ring.y) <= (RING_DIST/6) and abs(ring.z - p.mo.z) < (RING_DIST/6)
                soup = p.mo
            end
        end
        if soup
            local momRing = P_SpawnMobjFromMobj(ring, 0,0,0, MT_FLINGRING)
            momRing.followmo = soup
            P_RemoveMobj(ring)
        end
    end
end, MT_RING)

addHook("MobjThinker", function(ring)
    if ring and ring.valid and ring.health > 0 and ring.followmo and ring.followmo.valid
        if R_PointToDist2(ring.followmo.x, ring.followmo.y, ring.x, ring.y) <= RING_DIST/3 and abs(ring.z - ring.followmo.z) < RING_DIST/3
			SuperAttract(ring, ring.followmo)
        else
            ring.fuse = 5*TICRATE
        end
        if ring.fuse == 2
            P_SpawnMobjFromMobj(ring, 0,0,0, MT_RING)
            P_RemoveMobj(ring)
        end
    end
end, MT_FLINGRING)

addHook("TouchSpecial", function(mo, toucher)
  if gamemap >= 123 and gamemap <= 135 return true end --don't want players interacting with the emerald
end, MT_EMERALD1)

addHook("TouchSpecial", function(mo, toucher)
  if gamemap >= 123 and gamemap <= 135 return true end
end, MT_EMERALD2)

addHook("TouchSpecial", function(mo, toucher)
  if gamemap >= 123 and gamemap <= 135 return true end
end, MT_EMERALD3)

addHook("TouchSpecial", function(mo, toucher)
  if gamemap >= 123 and gamemap <= 135 return true end
end, MT_EMERALD4)

addHook("TouchSpecial", function(mo, toucher)
  if gamemap >= 123 and gamemap <= 135 return true end
end, MT_EMERALD5)

addHook("TouchSpecial", function(mo, toucher)
  if gamemap >= 123 and gamemap <= 135 return true end
end, MT_EMERALD6)

addHook("TouchSpecial", function(mo, toucher)
  if gamemap >= 123 and gamemap <= 135 return true end
end, MT_EMERALD7)

--goalring support for netgames
addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end

	if mobj.tracer.tracer  --tracers of tracers are dumb.
	and mobj.target
		if mobj.tracer.tracer.signglow == true
			mobj.tracer.tracer.blendmode = AST_ADD
		else
			mobj.tracer.tracer.blendmode = 0
		end
	end
	--if gamemap < 123 return end //only mudhole karst has a shrine rn, so only it needs this function
	
	--if gamemap > 129 return end
	
	if gamemap != 123 return end
	
	if GoalRing == nil
		P_RemoveMobj(mobj)
	end
end, MT_SIGN)

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if gamemap != 125 return end
	if GoalRing == nil
		P_RemoveMobj(mobj)
	end
end, MT_EMERALD3)

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if gamemap != 126 return end
	if GoalRing == nil
		P_RemoveMobj(mobj)
	end
end, MT_EMERALD4)

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if gamemap != 127 return end
	if GoalRing == nil
		P_RemoveMobj(mobj)
	end
end, MT_EMERALD5)

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if gamemap != 128 return end
	if GoalRing == nil
		P_RemoveMobj(mobj)
	end
end, MT_EMERALD6)

--thx inferno
--despawn ring maces in ultimate mode
addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	
	--if (mapheaderinfo[gamemap].lvlttl == "Dimension Warp")
	--and not netgame then
		 --P_RemoveMobj(mobj)
	-- end
	 
	if ultimatemode and mobj.hprev
		if mobj.hprev.type == MT_RING and mobj.hprev.type == MT_CUSTOMMACEPOINT
			P_RemoveMobj(mobj)
		end
	end
end, MT_RING)


addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if not netgame return end
    if (mapheaderinfo[gamemap].lvlttl != "Dimension Warp") return end
	
	P_RemoveMobj(mobj)
	
end, MT_URING)

addHook("PlayerSpawn", function(p)
	if not p.realmo return false end
	if p.spectator return false end
	if p.mo.signglow == nil
		p.mo.signglow = false
	end
	if p.mrce == nil
		local mrce = {
		glowaura = false,
		flycheat = false,
		hypercheat = false,
		canhyper = false,
		ultrastar = false,
		hyperimages = false,
		hypermode = false,
		customskin = 0,
		dontwantphysics = false,
		physics = true,
		skipmystic = false,
		nasyamystic = false,
		exspark = false,
		ishyper = false,
		jump = 0
		}
		if p.mo
			p.mrce = mrce
		end
	end
	if not netgame
		if io and p == consoleplayer and p.screenflash == nil
			local file = io.openlocal("client/mrce/hyperflash.dat")
			if file
				local string = file:read("*a")
				if string == "1" or string == "true" or string == "on"
					COM_BufInsertText(p, "hyperflash on")
				elseif string == "0" or string == "false" or string == "off" or string == nil
					COM_BufInsertText(p, "hyperflash off")
				end
				file:close()
			end
		end
	end
	--give super to all characters in dwz except those that break
    if p.mo and p.mo.valid and (mapheaderinfo[gamemap].lvlttl == "Dimension Warp") and not (p.mo.skin == "sms" or p.mo.skin == "msonic")
        if not (p.charflags & SF_SUPER)
            p.charflags = $1|SF_SUPER
        end
    end
--reset hyper values
	p.mrce.canhyper = false
	p.mrce.ultrastar = false
	p.mrce.hyperimages = false
	p.skincheck = p.mo.skin
	
	if pmo and p.mo.valid and p.mo.skin == "supersonic" and ((gamemap == 132)  or (gamemap == 133))
		P_GivePlayerRings(p, 25)
	end
end)

//credit to katsy for this tiny function pulled from reveries. it's so simple I could've written it myself, but eh.
--Makes elemental shield not render its fire when in water. Because fire can't burn in water. Obviously.

addHook("MobjThinker", function(shield)
	if (shield.target) and shield.target.valid
		if (shield.target.type == MT_ELEMENTAL_ORB)
			if ((shield.target.target.eflags & MFE_UNDERWATER or shield.target.target.eflags & MFE_TOUCHWATER) and not (shield.target.target.eflags & MFE_TOUCHLAVA))
				shield.flags2 = $|MF2_DONTDRAW
			elseif (shield.flags2 & MF2_DONTDRAW)
				shield.flags2 = $ & ~MF2_DONTDRAW
			end
		end
	end
end, MT_OVERLAY)

//cheat codes. they're mostly here for debug purposes, but they're fun to mess with, so I'll likely be keeping them here.
//currently only 2 cheats are present; hyper cheat, which gives all 7 emeralds and unlocks hyper form, 
//and fly cheat, which allows super sonic to fly outside of dwz
//update, glow aura adds blendmode aura to sonic's rebound dash , jump ball, spindash, and roll. Was maybe thinking as a 100% complete reward, until then, it's an easter egg

COM_AddCommand("mrsecret", function(player, arg)
	if (gamestate == GS_LEVEL) and player.valid
		if arg and arg == "4126"
			if not netgame
				player.mrce.hypercheat = true
				if modifiedgame == false
					COM_BufInsertText(player, "devmode 1")
					COM_BufInsertText(player, "devmode 0")
				end
			else
				print(player.name .. " is a sussy baka")
				P_DamageMobj(player.mo,nil,nil,1,DMG_INSTAKILL)
			end
		elseif arg == "20071101" and (player == consoleplayer) and not netgame
			player.mrce.flycheat = true
		elseif arg == "0" or arg == "off"
			player.mrce.flycheat = false
			player.mrce.hypercheat = false
			player.mrce.exspark = false
			player.mrce.glowaura = false
		elseif arg == "20100523"
			if player.mrce.exspark
				player.mrce.exspark = false
			else
				player.mrce.exspark = true
			end
		elseif arg == "1207" or arg == "glow"
			if player.mrce.glowaura == true
				--player.mo.signglow = false
				player.mrce.glowaura = false
				player.mo.colorized = false
				player.mo.blendmode = 0
				if player.pet
					player.mo.pet.blendmode = 0
					player.mo.pet.colorized = false
				end
				if player.buddies
					for id,buddy in pairs(player.buddies) do
						if buddy.mo and buddy.mo.valid
							buddy.mo.blendmode = 0
							buddy.mo.colorized = false
						end
					end
				end
			elseif player.mrce.glowaura == false or player.mrce.glowaura == nil
				player.mrce.glowaura = true
				--player.mo.signglow = true
				if player.spinitem == MT_THOK
					player.spinitem = 0
				end
				if player.pet
					player.mo.pet.blendmode = AST_ADD
					if not (player.mo.pet.color == SKINCOLOR_NONE)
						player.mo.pet.colorized = true
					end
				end
			end
		else
			CONS_Printf(player, "\133invalid input")
	//	else
	//		print("You can't use this in a netgame")
		end
	end
end, 0)

rawset(_G, "MRCE_superSpark", function(mo, amount, fuse, speed1, speed2, hyper, usecolor)
	local colors = {SKINCOLOR_EMERALD, SKINCOLOR_PURPLE, SKINCOLOR_BLUE, SKINCOLOR_CYAN, SKINCOLOR_ORANGE, SKINCOLOR_RED, SKINCOLOR_GREY}
	if not (mo and mo.valid and amount ~= nil and fuse ~= nil and speed1 ~= nil and speed2 ~= nil and hyper ~= nil) then
		return
	end

	for i = 0, amount, 1 do
		local ha = P_RandomRange(0, 360)*ANG1
		local va = P_RandomRange(0, 360)*ANG1
		local speed = P_RandomRange(speed1/FRACUNIT, speed2/FRACUNIT)*FRACUNIT

		local sprk = P_SpawnMobj(mo.x + FixedMul(FixedMul(mo.radius, FixedMul(cos(ha), cos(va))), mo.scale),
								 mo.y + FixedMul(FixedMul(mo.radius, FixedMul(sin(ha), cos(va))), mo.scale),
								 mo.z + FixedMul(FixedMul(mo.radius, sin(va)), mo.scale) + FixedMul(mo.scale, mo.height/2),
								 MT_SUPERSPARKLES)

		sprk.scale = mo.scale
		sprk.fuse = fuse
		if hyper == false then
			if usecolor == 0 then
				if mo.color then
					sprk.color = mo.color
				else
					sprk.color = 60
				end
			else
				sprk.color = usecolor
			end
		else
			sprk.color = colors[P_RandomRange(1, #colors)]
		end
		if mo.player and (mo.player == displayplayer) and not CV_FindVar("chasecam").value then
			sprk.flags2 = $|MF2_DONTDRAW
		end

		sprk.momx = FixedMul(FixedMul(speed, FixedMul(cos(ha), cos(va))), mo.scale)
		sprk.momy = FixedMul(FixedMul(speed, FixedMul(sin(ha), cos(va))), mo.scale)
		sprk.momz = FixedMul(FixedMul(speed, sin(va)), mo.scale)
	end
end)

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if (emeralds & EMERALD1) != 1
		P_RemoveMobj(mobj)
	end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE)
		MRCE_superSpark(mobj, 1, 5, 1, 21845, false, 55)
	end
	mobj.blendmode = AST_ADD
end, MT_FAKEEMERALD1)

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if (emeralds & EMERALD2) != 2
		P_RemoveMobj(mobj)
	end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE)
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 62)
	end
	mobj.blendmode = AST_ADD
end, MT_FAKEEMERALD2)

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if (emeralds & EMERALD3) != 4
		P_RemoveMobj(mobj)
	end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE)
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 43)
	end
	mobj.blendmode = AST_ADD
end, MT_FAKEEMERALD3)

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if (emeralds & EMERALD4) != 8
		P_RemoveMobj(mobj)
	end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE)
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 23)
	end
	mobj.blendmode = AST_ADD
end, MT_FAKEEMERALD4)

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if (emeralds & EMERALD5) != 16
		P_RemoveMobj(mobj)
	end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE)
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 50)
	end
	mobj.blendmode = AST_ADD
end, MT_FAKEEMERALD5)

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if (emeralds & EMERALD6) != 32
		P_RemoveMobj(mobj)
	end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE)
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 36)
	end
	mobj.blendmode = AST_ADD
end, MT_FAKEEMERALD6)

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if (emeralds & EMERALD7) != 64
		P_RemoveMobj(mobj)
	end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE)
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 3)
	end
	mobj.blendmode = AST_ADD
end, MT_FAKEEMERALD7)

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE)
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 55)
	end
	mobj.blendmode = AST_ADD
end, MT_EMERALD1)

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE)
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 62)
	end
	mobj.blendmode = AST_ADD
	if gamemap == 124
		if GoalRing == nil
			P_RemoveMobj(mobj)
		end
	end
end, MT_EMERALD2)

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE)
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 43)
	end
	mobj.blendmode = AST_ADD
end, MT_EMERALD3)

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE)
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 23)
	end
	mobj.blendmode = AST_ADD
end, MT_EMERALD4)

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE)
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 50)
	end
	mobj.blendmode = AST_ADD
end, MT_EMERALD5)

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE)
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 36)
	end
	mobj.blendmode = AST_ADD
end, MT_EMERALD6)

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	if (leveltime%TICRATE == TICRATE/2) or (leveltime%TICRATE == TICRATE)
		MRCE_superSpark(mobj, 1, 5, 1, 1*FRACUNIT, false, 3)
	end
	mobj.blendmode = AST_ADD
end, MT_EMERALD7)

--air bubble thinker
addHook("TouchSpecial", function(mo, toucher)
  if toucher.player.hypermode == 1 return true -- hyper forms don't need bubbles
  elseif (toucher.player.yusonictable and toucher.player.yusonictable.hypersonic and toucher.player.mo.skin == "adventuresonic") return true
  else toucher.player.powers[pw_spacetime] = 12 * TICRATE end --midnight freeze's ice water uses space countdown
end, MT_EXTRALARGEBUBBLE)

--break spikes
local function Break_shit (plr, thing)
	if (thing.type == MT_SPIKE
	or thing.type == MT_WALLSPIKE)
	then
		if (thing.flags & MF_SOLID) and ((thing.type == MT_SPIKE) or (thing.type == MT_WALLSPIKE))
		then
			S_StartSound(plr, thing.info.deathsound)
		end
		for iter in thing.subsector.sector.thinglist()
		do
			if (
				iter.type == thing.type and
				iter.health > 0 and
				( iter.flags & MF_SOLID ) and
				(iter == thing or
				P_AproxDistance(P_AproxDistance(
				thing.x - iter.x, thing.y - iter.y),
				thing.z - iter.z) < 56*thing.scale))
			then
				P_KillMobj(iter, plr, tmthing, 0)
			end
		end
		return 2
		elseif (( thing.flags & MF_MONITOR ))
		then
		if (P_DamageMobj(thing, tmthing, tmthing, 1, 0))
		then
			return 2
		end
	end
end

//break spikes with hyper form
addHook ("MobjMoveCollide", function (plr, thing)
	if plr.player ~= nil and (plr.skin == "sonic")
	or plr.player ~= nil and ((plr.player.mrce.canhyper == true) or (plr.player.mrce.ultrastar == true))
		local dm_threshold = (3*TICRATE)
		if plr and plr.player.powers[pw_super] and mrce_hyperunlocked
			if (thing.type == MT_SPIKE
			or thing.type == MT_WALLSPIKE)
				if (plr.z + plr.height < thing.z) 
				or (plr.z > thing.z + thing.height)
					return false
				else
					return Break_shit(plr, thing)
				end
			end
		end
	end
end)

//Wow this mod's been passed around a lot. Originally based on hyper abilities v4.1.2, then edited for mrce. 
//It barely maintains any of the original code, but credit is still due.  v4.1.2 by GameBoyTM101
//Credits for the original scripts from 2.1 go to MotdSpork on the SRB2 Message Board, as well as HitKid61/HitCoder
//Additional credits to Radicalicious for the Multiglide Knuckles and Infinite Flight Tails code.
//Also credit to DirkTheHusky for the custom colors themed after the 7 emeralds

rawset(_G, "mrce_hyperunlocked", false) --this is here for mrce to detect if hyper has been unlocked
local mrce_dowarptime = false --check if the post AGZ4 return warp is active

freeslot("SKINCOLOR_GALAXY")

local luabanks = reserveLuabanks() --save shit to the current savefile, not globally

skincolors[SKINCOLOR_GALAXY] = {
	name = "Galaxy",
	ramp = {178,162,163,165,166,167,167,157,158,158,159,253,253,30,30,31},
	invcolor = SKINCOLOR_SUNSET,
	invshade = 4,
	chatcolor = V_PURPLEMAP,
	accessible = true
}

//when you have sensitive eyes, this command allows to disable screen flashing
COM_AddCommand("hyperflash", function(player, arg)
	if arg
		if arg == "1" or arg == "true" or arg == "on"
			if io and player == consoleplayer
				local file = io.openlocal("client/mrce/hyperflash.dat", "w+")
				file:write(arg)
				file:close()
			end
			player.screenflash = true
			CONS_Printf(player, "Hyper form screen flashing has been enabled.")
		elseif arg == "0" or arg == "false" or arg == "off"
			if io and player == consoleplayer
				local file = io.openlocal("client/mrce/hyperflash.dat", "w+")
				file:write(arg)
				file:close()
			end	
			player.screenflash = false
			CONS_Printf(player, "Hyper form screen flashing has been disabled.")
		elseif player.screenflash
			CONS_Printf(player, "hyperflash [on/off] - Toggles screen flashing caused by hyper forms. Currently on.")
		else
			CONS_Printf(player, "hyperflash [on/off] - Toggles screen flashing caused by hyper forms. Currently off.")
		end
	elseif player.screenflash
		CONS_Printf(player, "hyperflash [on/off] - Toggles screen flashing caused by hyper forms. Currently on.")
	else
		CONS_Printf(player, "hyperflash [on/off] - Toggles screen flashing caused by hyper forms. Currently off.")
	end
end, 1)

//handle unlocking hyper, also gives knux and tails super abilities
addHook("PreThinkFrame",do
    for p in players.iterate
        if not (p and p.mo and p.mo.valid) continue end
        if not p.powers[pw_super] and p.mo.skin == "knuckles" and mrce_hyperunlocked == true
            p.charflags = $ & ~SF_MULTIABILITY
        end
        if p.powers[pw_super] and p.mo.skin == "knuckles" and mrce_hyperunlocked == true
            p.charflags = $1 | SF_MULTIABILITY
        end
        if p.powers[pw_super] and p.pflags & PF_THOKKED
            local ca = p.charability
            if ca == CA_FLY or ca == CA_SWIM
                p.powers[pw_tailsfly] = 8*TICRATE
            end
        elseif p.powers[pw_super]
            p.powers[pw_tailsfly] = 0
        end
		if ((gamemap == 130 and (p.pflags & PF_FINISHED)) and All7Emeralds(emeralds) and mrce_hyperunlocked == false)
			S_StartSound(null, sfx_s3k9c)
			luabanks[0] = 1
			mrce_hyperunlocked = true
		end
		if luabanks[0] == 1
		or p.mrce.hypercheat == true
			mrce_hyperunlocked = true
			if p.hyper and not p.hyper.isunlocked
				p.hyper.isunlocked = true
			end
			if p.yusonictable and not p.yusonictable.hyperpower and p.mo.skin == "adventuresonic" and p.mo.health
				p.yusonictable.hyperpower = true
			end
		else
			mrce_hyperunlocked = false
		end
		if gamemap == 122 and p.pflags & PF_FINISHED --we're in agz's warp room, and are warping to another zone
			luabanks[1] = 1
			mrce_dowarptime = true --initialize warp memory
			--print("less goooooo")
		end
		if luabanks[1] == 1
			mrce_dowarptime = true --remember warp memory on reloading the save
			--print("yup")
		else
			mrce_dowarptime = false
			--print("nope")
		end
		if gamemap == 132 --we made it to prismatic angel
			luabanks[1] = 0
			mrce_dowarptime = false --reset the value
		end
    end
end)

addHook("ThinkFrame", do
	for player in players.iterate
		if not player.realmo return false end
		if player.spectator return false end
		if (player.mo.eflags & MFE_VERTICALFLIP) and (CV_FindVar("flipcam").value)
			COM_BufInsertText(p, "flipcam Off")
		end
		if player.mo.skin == "supersonic"
			player.charability = 18
			if gamemap == 98 or gamemap == 99
				if not (leveltime%35)
				and not (player.bot)
				and not (player.mo.state >= S_PLAY_SUPER_TRANS1) and (player.mo.state <= S_PLAY_SUPER_TRANS6)
					P_GivePlayerRings(player, 1)
				end
			end
		end
		if player.mo and (player.mo.skin == "sonic" or player.mo.skin == "modernsonic")
			player.mrce.hyperimages = true
		end
		if player.mo and (player.mo.skin == "sonic" or player.mo.skin == "modernsonic" or player.mrce.canhyper == true)
			if player.powers[pw_super]
				if not player.mo.hypersparklocation
					player.mo.hypersparklocation = 0
				end
				player.mo.hypersparklocation = $1 + 14*FRACUNIT
			
				if not player.mo.hyperflashcolor
					player.mo.hyperflashcolor = 0
				end
			
				player.mo.hyperflashcolor = $1+1
			
				if player.mo.hyperflashcolor > 59
					player.mo.hyperflashcolor = 1
				end
			end
		end
	end
end)

//Super sparks made by HitKid61/HitCoder: Thank you :)
addHook("ThinkFrame", do
	for player in players.iterate
		if player.mo and (player.mo.skin == "sonic" or player.mo.skin == "modernsonic" or player.mrce.canhyper == true)
			
//			if player.custom1down == nil
//				player.custom1down = 0
//			end
			if not (player.hypermode)
			or player.powers[pw_super] == 0
			or player.exiting
			or player.mo.health == 0
				player.hypermode = 0
			end
//			if P_IsObjectOnGround(player.mo)
//			and player.cmd.buttons & BT_CUSTOM1
//			and player.hypermode == 0
//				player.custom1down = 1
//			end
//			if not (player.cmd.buttons & BT_CUSTOM1)
//				player.custom1down = 0
//			end
//			if player.mo.state >= S_PLAY_SUPER_TRANS1
//			and player.mo.state <= S_PLAY_SUPER_TRANS6
//				player.mo.momz = 0
//				player.powers[pw_nocontrol] = 1
//			end
			if player.powers[pw_super]
//			and player.cmd.buttons & BT_CUSTOM1
//			and player.custom1down == 0
			and player.mo.health
			and player.hypermode == 0
			and mrce_hyperunlocked == true
			--and not (player.pflags & PF_THOKKED)
				player.pflags = $1|PF_THOKKED
				player.hypermode = 1
//				player.custom1down = 1
//				player.mo.state = S_PLAY_SUPER_TRANS1
				S_StartSound(player.mo, sfx_supert)
			end
//			if  player.cmd.buttons & BT_CUSTOM1
//			and player.custom1down == 0
//			and player.powers[pw_super] 
//			and player.hypermode == 1
//			and not (player.pflags & PF_THOKKED)
//				player.pflags = $1|PF_THOKKED
//				player.hypermode = 0
//				S_StartSound(player.mo, sfx_s3k66)
//				player.custom1down = 1
//			end
			

			if player.powers[pw_super]
			and mrce_hyperunlocked
			and player.mrce.canhyper == true
				player.powers[pw_underwater] = 0
				player.powers[pw_spacetime] = 0

				/*local hyperparticle = P_SpawnMobjFromMobj(player.mo, P_RandomRange(-10, 10) * FRACUNIT, P_RandomRange(-10, 10) * FRACUNIT, 20*FRACUNIT, MT_SUPERSPARK) 
				hyperparticle.momx = P_RandomRange(-9, 9) * FRACUNIT
				hyperparticle.momy = P_RandomRange(-9, 9) * FRACUNIT
				hyperparticle.momz =  P_RandomRange(-9, 9) * FRACUNIT
				hyperparticle.colorized = true
				if P_RandomRange(1, 2) == 1
					hyperparticle.color = SKINCOLOR_YELLOW
				else
					hyperparticle.color = SKINCOLOR_CYAN
				end
				hyperparticle.fuse = 10
				hyperparticle.scale = player.mo.scale *2/3
				hyperparticle.source = player.mo*/

				if player.mo.hyperflashcolor < 5
				or player.mo.hyperflashcolor == 69
					player.mo.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
			
				if player.mo.hyperflashcolor > 4
				and player.mo.hyperflashcolor < 7
					player.mo.color = SKINCOLOR_SUPERSAPPHIRE1
					player.color = SKINCOLOR_SUPERSAPPHIRE1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end

				if player.mo.hyperflashcolor > 6
				and player.mo.hyperflashcolor < 9
					player.mo.color = SKINCOLOR_SUPERSAPPHIRE2
					player.color = SKINCOLOR_SUPERSAPPHIRE2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end

				if player.mo.hyperflashcolor > 8
				and player.mo.hyperflashcolor < 11
					player.mo.color = SKINCOLOR_SUPERSAPPHIRE1
					player.color = SKINCOLOR_SUPERSAPPHIRE1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 10
				and player.mo.hyperflashcolor < 13
					player.mo.color = SKINCOLOR_BLANK
					player.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 12
				and player.mo.hyperflashcolor < 15
					player.mo.color = SKINCOLOR_SUPERBUBBLEGUM1
					player.color = SKINCOLOR_SUPERBUBBLEGUM1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 14
				and player.mo.hyperflashcolor < 17
					player.mo.color = SKINCOLOR_SUPERBUBBLEGUM2
					player.color = SKINCOLOR_SUPERBUBBLEGUM2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 16
				and player.mo.hyperflashcolor < 19
					player.mo.color = SKINCOLOR_SUPERBUBBLEGUM1
					player.color = SKINCOLOR_SUPERBUBBLEGUM1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 18
				and player.mo.hyperflashcolor < 21
					player.mo.color = SKINCOLOR_BLANK
					player.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 20
				and player.mo.hyperflashcolor < 23
					player.mo.color = SKINCOLOR_SUPERMINT1
					player.color = SKINCOLOR_SUPERMINT1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 22
				and player.mo.hyperflashcolor < 25
					player.mo.color = SKINCOLOR_SUPERMINT2
					player.color = SKINCOLOR_SUPERMINT2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 24
				and player.mo.hyperflashcolor < 27
					player.mo.color = SKINCOLOR_SUPERMINT1
					player.color = SKINCOLOR_SUPERMINT1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 26
				and player.mo.hyperflashcolor < 29
					player.mo.color = SKINCOLOR_BLANK
					player.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 28
				and player.mo.hyperflashcolor < 31
					player.mo.color = SKINCOLOR_SUPERRUBY1
					player.color = SKINCOLOR_SUPERRUBY1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 30
				and player.mo.hyperflashcolor < 33
					player.mo.color = SKINCOLOR_SUPERRUBY2
					player.color = SKINCOLOR_SUPERRUBY2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 32
				and player.mo.hyperflashcolor < 35
					player.mo.color = SKINCOLOR_SUPERRUBY1
					player.color = SKINCOLOR_SUPERRUBY1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 34
				and player.mo.hyperflashcolor < 37
					player.mo.color = SKINCOLOR_BLANK
					player.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 36
				and player.mo.hyperflashcolor < 39
					player.mo.color = SKINCOLOR_SUPERWAVE1
					player.color = SKINCOLOR_SUPERWAVE1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 38
				and player.mo.hyperflashcolor < 41
					player.mo.color = SKINCOLOR_SUPERWAVE2
					player.color = SKINCOLOR_SUPERWAVE2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 40
				and player.mo.hyperflashcolor < 43
					player.mo.color = SKINCOLOR_SUPERWAVE1
					player.color = SKINCOLOR_SUPERWAVE1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 42
				and player.mo.hyperflashcolor < 45
					player.mo.color = SKINCOLOR_BLANK
					player.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 44
				and player.mo.hyperflashcolor < 47
					player.mo.color = SKINCOLOR_SUPERCOPPER1
					player.color = SKINCOLOR_SUPERCOPPER1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 46
				and player.mo.hyperflashcolor < 49
					player.mo.color = SKINCOLOR_SUPERCOPPER2
					player.color = SKINCOLOR_SUPERCOPPER2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 48
				and player.mo.hyperflashcolor < 51
					player.mo.color = SKINCOLOR_SUPERCOPPER1
					player.color = SKINCOLOR_SUPERCOPPER1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
				
				if player.mo.hyperflashcolor > 50
				and player.mo.hyperflashcolor < 53
					player.mo.color = SKINCOLOR_BLANK
					player.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
	
				if player.mo.hyperflashcolor > 52
				and player.mo.hyperflashcolor < 55
					player.mo.hyperflashcolor = 1
					player.mo.color = SKINCOLOR_SUPERAETHER1
					player.color = SKINCOLOR_SUPERAETHER1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
	
				if player.mo.hyperflashcolor > 54
				and player.mo.hyperflashcolor < 57
					player.mo.color = SKINCOLOR_SUPERAETHER2
					player.color = SKINCOLOR_SUPERAETHER2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
	
				if player.mo.hyperflashcolor > 56
				and player.mo.hyperflashcolor < 59
					player.mo.hyperflashcolor = 1
					player.mo.color = SKINCOLOR_SUPERAETHER1
					player.color = SKINCOLOR_SUPERAETHER1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 8
				end
			end
		end
		if player.mo 
			if player.powers[pw_super] and player.mrce.ultrastar == true and mrce_hyperunlocked == true
				player.powers[pw_underwater] = 0
				player.powers[pw_spacetime] = 0
			
				if not player.mo.ultraflashcolor
					player.mo.ultraflashcolor = 0
				end
			
				player.mo.ultraflashcolor = $1+1
			
				if player.mo.ultraflashcolor > 139
					player.mo.ultraflashcolor = 1
				end
			end
		end
		if player.mo and player.mrce.ultrastar == true and mrce_hyperunlocked == true
			if not (player.hypermode)
			or player.powers[pw_super] == 0
			or player.exiting
			or player.mo.health == 0
				player.hypermode = 0
			end		

			if player.powers[pw_super] and player.mrce.ultrastar == true and mrce_hyperunlocked == true
				if player.mo.ultraflashcolor < 2
				or player.mo.ultraflashcolor == 143
					player.mo.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
			
				if player.mo.ultraflashcolor > 1
				and player.mo.ultraflashcolor < 4
					player.mo.color = SKINCOLOR_SUPERMINT1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
			
				if player.mo.ultraflashcolor > 3
				and player.mo.ultraflashcolor < 6
					player.mo.color = SKINCOLOR_SUPERMINT2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 5
				and player.mo.ultraflashcolor < 8
					player.mo.color = SKINCOLOR_SUPERMINT3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 7
				and player.mo.ultraflashcolor < 10
					player.mo.color = SKINCOLOR_SUPERMINT4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 9
				and player.mo.ultraflashcolor < 12
					player.mo.color = SKINCOLOR_SUPERMINT5
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
	
				if player.mo.ultraflashcolor > 11
				and player.mo.ultraflashcolor < 14
					player.mo.color = SKINCOLOR_SUPERMINT4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 13
				and player.mo.ultraflashcolor < 16
					player.mo.color = SKINCOLOR_SUPERMINT3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 15
				and player.mo.ultraflashcolor < 18
					player.mo.color = SKINCOLOR_SUPERMINT2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 17
				and player.mo.ultraflashcolor < 20
					player.mo.color = SKINCOLOR_SUPERMINT1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 19
				and player.mo.ultraflashcolor < 22
					player.mo.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 21
				and player.mo.ultraflashcolor < 24
					player.mo.color = SKINCOLOR_SUPERBUBBLEGUM1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
			
				if player.mo.ultraflashcolor > 23
				and player.mo.ultraflashcolor < 26
					player.mo.color = SKINCOLOR_SUPERBUBBLEGUM2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 25
				and player.mo.ultraflashcolor < 28
					player.mo.color = SKINCOLOR_SUPERBUBBLEGUM3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 27
				and player.mo.ultraflashcolor < 30
					player.mo.color = SKINCOLOR_SUPERBUBBLEGUM4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 29
				and player.mo.ultraflashcolor < 32
					player.mo.color = SKINCOLOR_SUPERBUBBLEGUM5
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
	
				if player.mo.ultraflashcolor > 31
				and player.mo.ultraflashcolor < 34
					player.mo.color = SKINCOLOR_SUPERBUBBLEGUM4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 33
				and player.mo.ultraflashcolor < 36
					player.mo.color = SKINCOLOR_SUPERBUBBLEGUM3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 35
				and player.mo.ultraflashcolor < 38
					player.mo.color = SKINCOLOR_SUPERBUBBLEGUM2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 37
				and player.mo.ultraflashcolor < 40
					player.mo.color = SKINCOLOR_SUPERBUBBLEGUM1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 39
				and player.mo.ultraflashcolor < 42
					player.mo.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 41
				and player.mo.ultraflashcolor < 44
					player.mo.color = SKINCOLOR_SUPERSAPPHIRE1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
			
				if player.mo.ultraflashcolor > 43
				and player.mo.ultraflashcolor < 46
					player.mo.color = SKINCOLOR_SUPERSAPPHIRE2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 45
				and player.mo.ultraflashcolor < 48
					player.mo.color = SKINCOLOR_SUPERSAPPHIRE3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 47
				and player.mo.ultraflashcolor < 50
					player.mo.color = SKINCOLOR_SUPERSAPPHIRE4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 49
				and player.mo.ultraflashcolor < 52
					player.mo.color = SKINCOLOR_SUPERSAPPHIRE5
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
	
				if player.mo.ultraflashcolor > 51
				and player.mo.ultraflashcolor < 54
					player.mo.color = SKINCOLOR_SUPERSAPPHIRE4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 53
				and player.mo.ultraflashcolor < 56
					player.mo.color = SKINCOLOR_SUPERSAPPHIRE3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 55
				and player.mo.ultraflashcolor < 58
					player.mo.color = SKINCOLOR_SUPERSAPPHIRE2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 57
				and player.mo.ultraflashcolor < 60
					player.mo.color = SKINCOLOR_SUPERSAPPHIRE1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 59
				and player.mo.ultraflashcolor < 62
					player.mo.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 61
				and player.mo.ultraflashcolor < 64
					player.mo.color = SKINCOLOR_SUPERWAVE1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
			
				if player.mo.ultraflashcolor > 63
				and player.mo.ultraflashcolor < 66
					player.mo.color = SKINCOLOR_SUPERWAVE2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 65
				and player.mo.ultraflashcolor < 68
					player.mo.color = SKINCOLOR_SUPERWAVE3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 67
				and player.mo.ultraflashcolor < 70
					player.mo.color = SKINCOLOR_SUPERWAVE4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 69
				and player.mo.ultraflashcolor < 72
					player.mo.color = SKINCOLOR_SUPERWAVE5
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
	
				if player.mo.ultraflashcolor > 71
				and player.mo.ultraflashcolor < 74
					player.mo.color = SKINCOLOR_SUPERWAVE4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 73
				and player.mo.ultraflashcolor < 76
					player.mo.color = SKINCOLOR_SUPERWAVE3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 75
				and player.mo.ultraflashcolor < 78
					player.mo.color = SKINCOLOR_SUPERWAVE2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 77
				and player.mo.ultraflashcolor < 80
					player.mo.color = SKINCOLOR_SUPERWAVE1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 79
				and player.mo.ultraflashcolor < 82
					player.mo.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 81
				and player.mo.ultraflashcolor < 84
					player.mo.color = SKINCOLOR_SUPERCOPPER1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
			
				if player.mo.ultraflashcolor > 83
				and player.mo.ultraflashcolor < 86
					player.mo.color = SKINCOLOR_SUPERCOPPER2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 85
				and player.mo.ultraflashcolor < 88
					player.mo.color = SKINCOLOR_SUPERCOPPER3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 87
				and player.mo.ultraflashcolor < 90
					player.mo.color = SKINCOLOR_SUPERCOPPER4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 89
				and player.mo.ultraflashcolor < 92
					player.mo.color = SKINCOLOR_SUPERCOPPER5
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
	
				if player.mo.ultraflashcolor > 91
				and player.mo.ultraflashcolor < 94
					player.mo.color = SKINCOLOR_SUPERCOPPER4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 93
				and player.mo.ultraflashcolor < 96
					player.mo.color = SKINCOLOR_SUPERCOPPER3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 95
				and player.mo.ultraflashcolor < 98
					player.mo.color = SKINCOLOR_SUPERCOPPER2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 97
				and player.mo.ultraflashcolor < 100
					player.mo.color = SKINCOLOR_SUPERCOPPER1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 99
				and player.mo.ultraflashcolor < 102
					player.mo.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 101
				and player.mo.ultraflashcolor < 104
					player.mo.color = SKINCOLOR_SUPERRUBY1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
			
				if player.mo.ultraflashcolor > 103
				and player.mo.ultraflashcolor < 106
					player.mo.color = SKINCOLOR_SUPERRUBY2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 105
				and player.mo.ultraflashcolor < 108
					player.mo.color = SKINCOLOR_SUPERRUBY3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 107
				and player.mo.ultraflashcolor < 110
					player.mo.color = SKINCOLOR_SUPERRUBY4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 109
				and player.mo.ultraflashcolor < 112
					player.mo.color = SKINCOLOR_SUPERRUBY5
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
	
				if player.mo.ultraflashcolor > 111
				and player.mo.ultraflashcolor < 114
					player.mo.color = SKINCOLOR_SUPERRUBY4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 113
				and player.mo.ultraflashcolor < 116
					player.mo.color = SKINCOLOR_SUPERRUBY3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 115
				and player.mo.ultraflashcolor < 118
					player.mo.color = SKINCOLOR_SUPERRUBY2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 117
				and player.mo.ultraflashcolor < 120
					player.mo.color = SKINCOLOR_SUPERRUBY1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 119
				and player.mo.ultraflashcolor < 122
					player.mo.color = SKINCOLOR_BLANK
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 121
				and player.mo.ultraflashcolor < 124
					player.mo.color = SKINCOLOR_SUPERAETHER1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
			
				if player.mo.ultraflashcolor > 123
				and player.mo.ultraflashcolor < 126
					player.mo.color = SKINCOLOR_SUPERAETHER2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 125
				and player.mo.ultraflashcolor < 128
					player.mo.color = SKINCOLOR_SUPERAETHER3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 127
				and player.mo.ultraflashcolor < 130
					player.mo.color = SKINCOLOR_SUPERAETHER4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 129
				and player.mo.ultraflashcolor < 132
					player.mo.color = SKINCOLOR_SUPERAETHER5
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
	
				if player.mo.ultraflashcolor > 131
				and player.mo.ultraflashcolor < 134
					player.mo.color = SKINCOLOR_SUPERAETHER4
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 133
				and player.mo.ultraflashcolor < 136
					player.mo.color = SKINCOLOR_SUPERAETHER3
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 135
				and player.mo.ultraflashcolor < 138
					player.mo.color = SKINCOLOR_SUPERAETHER2
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 137
				and player.mo.ultraflashcolor < 140
					player.mo.color = SKINCOLOR_SUPERAETHER1
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
				
				if player.mo.ultraflashcolor > 139
				and player.mo.ultraflashcolor < 142
					player.mo.color = SKINCOLOR_BLANK
					player.mo.ultraflashcolor = 0
					--local ghost = P_SpawnGhostMobj(player.mo)
					--ghost.fuse = 1
				end
			end
		end
	end
end)

local SkipLoaded = false
addHook("ThinkFrame", do
	if not SkipLoaded and AddSkipMonitor
		//Add a monitor to Skip's crafting list!
		/*The parameters are the monitor type, the scrap cost,
		the first line of the description, and the second line of the
		description, in that order.*/
		--AddSkipMonitor(MT_THEMYSTICEMERALD, 1, "\x82 Why:\x80 do you need", "\x82 to:\x80 craft this right now?")
		//Sets the amount of scrap the specified enemy rewards when hit.
		//Note: This only works for objects with MF_ENEMY and MF_BOSS.
		//SkipBadnikScrap[MT_BLUECRAWLA] = 32
		//Sets the skincolor of the scrap the specified enemy drops.
		//Note: Ditto.
		//SkipBadnikColor[MT_BLUECRAWLA] = SKINCOLOR_YELLOW
		//Set the script as loaded!
		SkipLoaded = true
	end
end)
--here it is. our really big playerthink hook
addHook("PlayerThink", function(p)
	if p.spectator return false end
	if not p.realmo return false end
	local x = p.mrce
--detect if physics are enabled
	if IsCustomSkin(p) 
		x.physics = false
	else
		x.physics = true
	end

	if mrce_hyperunlocked == true and p.yusonictable and p.yusonictable.supersonic and p.mo.skin == "adventuresonic" then
		p.yusonictable.hypersonic = true
	end
	
	if mrce_hyperunlocked == true and p.powers[pw_super]
	and x.canhyper or x.ultrastar then
		x.ishyper = true
	else
		x.ishyper = false
	end
	
	if p.mo and p.mo.skin == "sonic" then
		p.spinitem = 0
	end
	
--disable custom physics for a few known characters
	if p.mo and ((p.mo.skin == "sms") or (p.mo.skin == "ass") or (p.mo.skin == "juniosonic") or (p.mo.skin == "iclyn") or (p.mo.skin =="kiryu") or /*(p.mo.skin == "ryder") or */(p.mo.skin == "adventuresonic"))
		x.customskin = 2
		x.physics = false
	elseif x.dontwantphysics == false
		x.physics = true
	end


--post AGZ4 return warp handler
	if (gamemap == 103 or gamemap == 106 or gamemap == 109 or gamemap == 112 or gamemap == 115 or gamemap == 118)
	and mrce_dowarptime == true then
		if debug == 1
			print("go time")
		end
		G_SetCustomExitVars(122,1)
	end
	
--in ultimate mode, take away all rings unless in dwz
	if p.rings > 0 and ultimatemode and (mapheaderinfo[gamemap].lvlttl != "Dimension Warp") then
		p.rings = 0
	end

--fall animations. bc fuck airwalk, all my homies hate airwalk
	
	--
	if p.waitaminute == nil or P_IsObjectOnGround(p.mo) then
		p.waitaminute = 4 -- dont be too aggressive, otherwise it looks jank
	end
	if P_IsObjectOnGround(p.mo) == false and p.waitaminute > 0 then
		p.waitaminute = $ - 1
	end
	
	if p.mo.state == S_PLAY_STND or p.mo.state == S_PLAY_WAIT or p.mo.state == S_PLAY_WALK or p.mo.state == S_PLAY_RUN
		if p.mo.skin != "nasya" and p.mo.skin != "ssnsonic" and p.mo.skin != "srbii" and p.mo.skin != "adventuresonic" and not IsCustomSkin(p)
			if not P_IsObjectOnGround(p.mo)
			and p.powers[pw_carry] != CR_ROLLOUT
			and p.powers[pw_carry] != CR_MINECART
			and p.powers[pw_carry] != CR_MACESWING
			and p.powers[pw_carry] != CR_ZOOMTUBE
			and p.powers[pw_carry] != CR_ROPEHANG
			and p.powers[pw_carry] != CR_PLAYER
			and p.powers[pw_carry] != CR_NIGHTSMODE
			and p.waitaminute < 1 -- neon's edit to fix slope nonsense
				if debug == 1
					print("fall time")
				end
				if p.mo.momz >= 8
					p.mo.state = S_PLAY_SPRING
				else if p.mo.momz <= -8
					p.mo.state = S_PLAY_FALL
					end
				end
			end
		end
	end
	
--if p.speed == go fast, allow the player to run on water. aka, momentum based water running
	if (p.speed >= 60*FRACUNIT) and not (skins[p.mo.skin].flags & SF_RUNONWATER)  and not IsCustomSkin(p) 
		p.charflags = $1|SF_RUNONWATER
	elseif not IsCustomSkin(p) 
		if not (skins[p.mo.skin].flags & SF_RUNONWATER) and p.speed <= 60*FRACUNIT
			p.charflags = $1 & ~(SF_RUNONWATER)
		end
	end
	
	x.canhyper = $ or false
	x.hyperimages = $ or false
	x.ultrastar = $ or false
	--super tails?
	if (p.mo.skin == "tails" or p.mo.skin == "amy" or p.mo.skin == "fang") and (mrce_hyperunlocked == true or (mapheaderinfo[gamemap].lvlttl == "Dimension Warp"))
		p.charflags = $1 | SF_SUPER --yes
	elseif (p.mo.skin == "tails" or p.mo.skin == "amy" or p.mo.skin == "fang") and mrce_hyperunlocked == false
		p.charflags =  $ & ~SF_SUPER --nah
	end
	if p.powers[pw_super] and p.pflags & PF_THOKKED and p.mo.skin == "tails"
		p.powers[pw_tailsfly] = 8*TICRATE --super tails infinite fly
	end
	if p.powers[pw_super] and p.mo.skin == "tails"
		p.actionspd = skins[p.mo.skin].actionspd * 2
	elseif p.mo.skin == "tails"
		p.actionspd = skins[p.mo.skin].actionspd
	end
		
	--hyper modern sonic's boost fly doesn't drain additional rings unlike his super
	if p.powers[pw_super] and mrce_hyperunlocked == true
		if p.superboost == nil
			p.superboost = false
			p.boosting = false
		end
		if p.mo.skin == "modernsonic"
			if p.superboost == true
				if (leveltime % 5 == 0)             
				p.rings = p.rings + 1
				end
			end
			--also nukes because why the fuck not lol
			if (p.boosting == true or p.superboost == true) and (mapheaderinfo[gamemap].lvlttl != "Dimension Warp")
				if p.modernnuker == true
					P_NukeEnemies(p.mo, p.mo, 600*FRACUNIT)
					if p.screenflash == true
						P_FlashPal(p, PAL_WHITE, 7)
					end
					p.modernnuker = false
				end
			else
				p.modernnuker = true
			end
		end
	end
	
	--basic stat buffs for hyper form
	if p.mo.skin == "sonic" or x.canhyper == true or x.ultrastar == true
		if p.powers[pw_super] and p.hypermode == 1
			p.maxdash = skins[p.mo.skin].maxdash * 3 / 2
			p.jumpfactor = skins[p.mo.skin].jumpfactor * 7 / 6
			--p.supercolor = "Silver"  --doesn't actually work, the variable is read only. Maybe a request for 2.2.11?
			p.actionspd = skins[p.mo.skin].actionspd * 3 / 2
			p.mindash = 75 * FRACUNIT
			p.hyperstats = 1
		end
		if p.slowgooptimer == 0 and p.hypermode !=1
			p.maxdash = skins[p.mo.skin].maxdash
			p.jumpfactor = skins[p.mo.skin].jumpfactor
			--p.supercolor = "Gold"
			p.actionspd = skins[p.mo.skin].actionspd
			p.mindash = skins[p.mo.skin].mindash
			p.hyperstats = 0
		end
	end
	
--reset hyper values if the player changes skin
	if p.skincheck != p.mo.skin
		x.canhyper = false
		x.ultrastar = false
		x.hyperimages = false
		p.skincheck = p.mo.skin
	end
	--if player has max lives and gets a 1up, grant rings instead
	if p.powers[pw_extralife] == 2 and p.lives == 99 then
		P_GivePlayerRings(p, 100)
	end
	if x.exspark != false and p.playerstate != PST_DEAD and not (p.exiting and gamemap == 121 and not netgame)
		MRCE_superSpark(p.mo, 1, 8, 1, 5*FRACUNIT, false, 0)
	end
--give the player rings if they break a invinc monitor while super. Make lemonade out of lemons.
	if p.powers[pw_super] and p.powers[pw_invulnerability] and p.mo.skin != "supersonic"
		P_GivePlayerRings(p, 20)
		S_StartSound(null, sfx_itemup)
		p.powers[pw_invulnerability] = 0
	end
	
	if x.ultrastar == true and p.scoreadd < 4 and p.powers[pw_super] and mrce_hyperunlocked == true
		p.scoreadd = 4
	elseif P_IsObjectOnGround(p.mo) and p.powers[pw_invulnerability] == 0
		p.scoreadd = 0
	end
	
--spawn the hyper sparkles
	if p and p.valid and p.mo and p.mo.valid
		and p.mo.skin ~= blacklist
		and p.powers[pw_super]
		and mrce_hyperunlocked == true
		and ((x.canhyper == true) or (x.ultrastar == true))
		and not (p.mo.state >= S_PLAY_SUPER_TRANS1 and p.mo.state <= S_PLAY_SUPER_TRANS5) then
		MRCE_superSpark(p.mo, 1, 16, 1, 6*FRACUNIT, true, nil)
	end

--compatibility checks, allowing custom characters to have hyper forms
	if p.mo and p.mo.skin == "hms123311"
		if x.canhyper != nil
			x.canhyper = true
			x.hyperimages = true
		end
		if mrce_hyperunlocked == true
			p.pflags = $|PF_GODMODE
		else
			p.pflags = $ & ~PF_GODMODE
		end
	end
	local forwardmove = p.cmd.forwardmove
	if p.mo.skin == "mario" and p.powers[pw_super] and mrce_hyperunlocked == true
		if p.mariocapeflight != 0 and not P_IsObjectOnGround(p.mo)
			if forwardmove <= -35
				p.mo.momz = -50*P_GetMobjGravity(p.mo)
				if p.startultrafly == true
					S_StartSound(p.mo, sfx_mrfly)
					p.startultrafly = false
				end
			else p.startultrafly = true
			end
		end
		if p.mariogroundpound and  P_IsObjectOnGround(p.mo) and p.startpoundnuke == true
			P_NukeEnemies(p.mo, p.mo, 600*FRACUNIT)
			p.startpoundnuke = false
			if p.screenflash == true
				P_FlashPal(p, PAL_WHITE, 7)
			end
		elseif not P_IsObjectOnGround(p.mo)
			p.startpoundnuke = true
		end
/*		p.bljspeed = $ or 0
		p.endblj = $ or 5
		if p.mariolongjump > 2 and p.mariolongjump < 7 and forwardmove <= -35
			P_SetObjectMomZ(p.mo, -85*FRACUNIT)
			p.bljspeed = $ + (p.speed/2)
		end
		if p.mariolongjump <= 4 and p.mariolongjump > 0 and  forwardmove <= -35
			local actionspd = FixedMul(p.mo.scale, p.bljspeed)
			P_InstaThrust(p.mo, p.mo.angle, actionspd)
		end
		if  P_IsObjectOnGround(p.mo)
			p.endblj = $ - 1
		else p.endblj = 8 end
		if p.endblj <= 0
			p.bljspeed = 0
		end*/
	end
--	if p.mo and p.mo.skin == "pointy"
--		p.mrce.hyperimages = false
--		p.mrce.canhyper = true
--		p.mrce.ultrastar = false
--	end
	if p.mo .skin == "tails"
	or p.mo.skin == "fang"
	or p.mo.skin == "amy"
	or p.mo.skin == "mcsonic"
	or p.mo.skin == "crystalsonic"
	or p.mo.skin == "altsonic"
	or p.mo.skin == "supersonic"
		x.hyperimages = false
		x.canhyper = false
		x.ultrastar = false
	end
	if p.mo.skin == "modernsonic"
	or p.mo.skin == "HMS123311"
	or p.mo.skin == "dirk"
	or p.mo.skin == "yoshi"
	or p.mo.skin == "sonic"
	or p.mo.skin == "pointy"
	or p.mo.skin == "shadow"
	or p.mo.skin == "flame"
	or p.mo.skin == "juniosonic"
	or p.mo.skin == "bandages"
	or p.mo.skin == "bean"
	or p.mo.skin == "greeneyesonic"
		x.hyperimages = true
		x.canhyper = true
		x.ultrastar = false
	end
	if p.mo.skin == "metalsonic"
	or p.mo.skin == "knuckles"
		x.hyperimages = true
		x.ultrastar = false
		x.canhyper = false
	end
	if p.mo.skin == "mario"
	or p.mo.skin == "luigi"
	or p.mo.skin == "surge"
		x.ultrastar = true
		x.hyperimages = false
		x.canhyper = false
	end
	if p.mo.skin == "jana"
	or p.mo.skin == "skip"
	or p.mo.skin == "eggman"
		x.ultrastar = true
		x.hyperimages = true
		x.canhyper = false
	end
	if p.mo.hyperflashcolor == nil
		 p.mo.hyperflashcolor = 1
	 end
	 
	if (x.canhyper == true or x.ultrastar == true)
	and ((p.powers[pw_super]) and (mrce_hyperunlocked == true))
		x.hypermode = true
		if p.mo.skin == "skip" --SKIP BOOM
		or p.mo.skin == "nasya"
			p.mysticsuper = true
		end
	else
		x.hypermode = false
		if p.mo.skin == "skip"
		or p.mo.skin == "nasya"
			p.mysticsuper = false
		end
	end
	if p.mo.skin == "skip" 
	and mrce_hyperunlocked == true
		p.charflags = $1 | SF_SUPER
	end
	if p.mo.skin == "shadow" and ((p.powers[pw_super]) and (mrce_hyperunlocked == true))
		if p.shadow and p.shadow.flags & SHF_SPEARCHARGE
			p.momz = 0
		end
	end
--blendmode secret
	if x.glowaura == true then
		p.mo.signglow = true
		p.mo.blendmode = AST_ADD
		if p.followmobj then
			p.followmobj.blendmode = AST_ADD
			p.followmobj.colorized = true
		end
		p.mo.colorized = true
		if p.pet and p.mo.pet
			p.mo.pet.blendmode = AST_ADD
			if not (p.mo.pet.color == SKINCOLOR_NONE)
				p.mo.pet.colorized = true
			end
		end
		if p.buddies
			for id,buddy in pairs(p.buddies) do
				if buddy.mo and buddy.mo.valid
					buddy.mo.blendmode = AST_ADD
					buddy.mo.colorized = true
				end
			end
		end
	end

--hyper related cheat code handling
	if x.hypercheat == true
		mrce_hyperunlocked = true
		if not All7Emeralds(emeralds)
			emeralds = 127
		end
	end
	if x.flycheat == true and modifiedgame == false and not netgame
		COM_BufInsertText(p, "devmode 1")
		COM_BufInsertText(p, "devmode 0")
	end

--hyper after images
	if (p.powers[pw_super] and (p.speed >= 8*FRACUNIT or p.mo.momz >= 3*FRACUNIT or p.mo.momz <= -3*FRACUNIT) and x.hyperimages == true) --does our character support hyper afterimages?
	and mrce_hyperunlocked == true
	and not p.mo.state == ((p.mo.state == S_PLAY_SUPER_TRANS1) or (p.mo.state == S_PLAY_SUPER_TRANS2) or (p.mo.state == S_PLAY_SUPER_TRANS3) or (p.mo.state == S_PLAY_SUPER_TRANS4) or (p.mo.state == S_PLAY_SUPER_TRANS5) or (p.mo.state == S_PLAY_SUPER_TRANS6))
		local AfterImageSpawn = P_SpawnGhostMobj(p.mo)
		AfterImageSpawn.colorized = true
		AfterImageSpawn.fuse = 4
		if p.mo.hyperflashcolor < 5
		or p.mo.hyperflashcolor == 69
			AfterImageSpawn.color = SKINCOLOR_BLANK
		end
		
		if p.mo.hyperflashcolor > 4
		and p.mo.hyperflashcolor < 7
			AfterImageSpawn.color = SKINCOLOR_SUPERSAPPHIRE1
		end
		
		if p.mo.hyperflashcolor > 6
		and p.mo.hyperflashcolor < 9
		AfterImageSpawn.color = SKINCOLOR_SUPERSAPPHIRE2
		end
		
		if p.mo.hyperflashcolor > 8
		and p.mo.hyperflashcolor < 11
			AfterImageSpawn.color = SKINCOLOR_SUPERSAPPHIRE1
		end
		
		if p.mo.hyperflashcolor > 10
		and p.mo.hyperflashcolor < 13
			AfterImageSpawn.color = SKINCOLOR_BLANK
		end
		
		if p.mo.hyperflashcolor > 12
		and p.mo.hyperflashcolor < 15
			AfterImageSpawn.color = SKINCOLOR_SUPERBUBBLEGUM1
		end
		
		if p.mo.hyperflashcolor > 14
		and p.mo.hyperflashcolor < 17
			AfterImageSpawn.color = SKINCOLOR_SUPERBUBBLEGUM2
		end
		
		if p.mo.hyperflashcolor > 16
		and p.mo.hyperflashcolor < 19
			AfterImageSpawn.color = SKINCOLOR_SUPERBUBBLEGUM1
		end
		
		if p.mo.hyperflashcolor > 18
		and p.mo.hyperflashcolor < 21
			AfterImageSpawn.color = SKINCOLOR_BLANK
		end
		
		if p.mo.hyperflashcolor > 20
		and p.mo.hyperflashcolor < 23
			AfterImageSpawn.color = SKINCOLOR_SUPERMINT1
		end
		
		if p.mo.hyperflashcolor > 22
		and p.mo.hyperflashcolor < 25
			AfterImageSpawn.color = SKINCOLOR_SUPERMINT2
		end
		
		if p.mo.hyperflashcolor > 24
		and p.mo.hyperflashcolor < 27
			AfterImageSpawn.color = SKINCOLOR_SUPERMINT1
		end
		
		if p.mo.hyperflashcolor > 26
		and p.mo.hyperflashcolor < 29
			AfterImageSpawn.color = SKINCOLOR_BLANK
		end
		
		if p.mo.hyperflashcolor > 28
		and p.mo.hyperflashcolor < 31
			AfterImageSpawn.color = SKINCOLOR_SUPERRUBY1
		end
		
		if p.mo.hyperflashcolor > 30
		and p.mo.hyperflashcolor < 33
			AfterImageSpawn.color = SKINCOLOR_SUPERRUBY2
		end
		
		if p.mo.hyperflashcolor > 32
		and p.mo.hyperflashcolor < 35
			AfterImageSpawn.color = SKINCOLOR_SUPERRUBY1
		end
		
		if p.mo.hyperflashcolor > 34
		and p.mo.hyperflashcolor < 37
			AfterImageSpawn.color = SKINCOLOR_BLANK
		end
		
		if p.mo.hyperflashcolor > 36
		and p.mo.hyperflashcolor < 39
			AfterImageSpawn.color = SKINCOLOR_SUPERWAVE1
		end
		
		if p.mo.hyperflashcolor > 38
		and p.mo.hyperflashcolor < 41
			AfterImageSpawn.color = SKINCOLOR_SUPERWAVE2
		end
		
		if p.mo.hyperflashcolor > 40
		and p.mo.hyperflashcolor < 43
			AfterImageSpawn.color = SKINCOLOR_SUPERWAVE1
		end
		
		if p.mo.hyperflashcolor > 42
		and p.mo.hyperflashcolor < 45
			AfterImageSpawn.color = SKINCOLOR_BLANK
		end
		
		if p.mo.hyperflashcolor > 44
		and p.mo.hyperflashcolor < 47
			AfterImageSpawn.color = SKINCOLOR_SUPERCOPPER1
		end
		
		if p.mo.hyperflashcolor > 46
		and p.mo.hyperflashcolor < 49
			AfterImageSpawn.color = SKINCOLOR_SUPERCOPPER2
		end
		
		if p.mo.hyperflashcolor > 48
		and p.mo.hyperflashcolor < 51
			AfterImageSpawn.color = SKINCOLOR_SUPERCOPPER1
		end
		
		if p.mo.hyperflashcolor > 50
		and p.mo.hyperflashcolor < 53
			AfterImageSpawn.color = SKINCOLOR_BLANK
		end
		
		if p.mo.hyperflashcolor > 52
		and p.mo.hyperflashcolor < 55
			AfterImageSpawn.color = SKINCOLOR_SUPERAETHER1
		end
		
		if p.mo.hyperflashcolor > 54
		and p.mo.hyperflashcolor < 57
			AfterImageSpawn.color = SKINCOLOR_SUPERAETHER2
		end
		
		if p.mo.hyperflashcolor > 56
		and p.mo.hyperflashcolor < 59
			AfterImageSpawn.color = SKINCOLOR_SUPERAETHER1
		end
	end
end)

addHook("PlayerCanDamage", function(player, mobj) --hyper forms can pop monitors by just walking into them
	if mrce_hyperunlocked != true return end
	if not player.powers[pw_super] return end
	if player.mrce.canhyper == true or player.mrce.ultrastar == true or (player.yusonictable and player.yusonictable.hypersonic and player.mo.skin == "adventuresonic") or (player.hyper and player.hyper.transformed)
		return true
	end
end)

addHook("MobjMoveCollide", function(monitor, plmo)
	if monitor and monitor.valid
	and (monitor.flags & MF_MONITOR)
	and not (plmo.player.ctfteam == 1 and monitor.type == MT_RING_BLUEBOX)
	and not (plmo.player.ctfteam == 2 and monitor.type == MT_RING_REDBOX)
	and (plmo.player)
	and player.hypermode and player.hypermode == 1
    and not (plmo.z > monitor.z+monitor.height+(10*FRACUNIT)) and not (monitor.z > plmo.z+plmo.height+(10*FRACUNIT))
	and monitor.valid and monitor.health
    local player = plmo.player
    and player.powers[pw_super]
        P_KillMobj(monitor, plmo, plmo)
		return false
	end
end, MT_PLAYER)

//Literally just a port from the source code aside from the slow fall part
addHook("JumpSpinSpecial", function(player)
	if player.hypermode and player.hypermode == 1 and (player.mo.skin == "sonic" or player.mo.skin == "supersonic")
	or player.hyper and player.hyper.transformed
	and player.speed < 5*player.mo.scale
	and P_MobjFlip(player.mo)*player.mo.momz <= 0
		if player.speed >= FixedMul(player.runspeed, player.mo.scale)
			player.mo.state = S_PLAY_FLOAT_RUN
		else
			player.mo.state = S_PLAY_FLOAT
		end
		P_SetObjectMomZ(player.mo, -3*FRACUNIT)
		player.pflags = $&~(PF_STARTJUMP|PF_SPINNING)
	end
	
	if player.powers[pw_super] and (player.mo.skin == "supersonic" or player.charability == 18)
	and player.speed > 5*player.mo.scale
	and P_MobjFlip(player.mo)*player.mo.momz <= 0
		if player.speed >= FixedMul(player.runspeed, player.mo.scale)
			player.mo.state = S_PLAY_FLOAT_RUN
		else
			player.mo.state = S_PLAY_FLOAT
		end
		P_SetObjectMomZ(player.mo, 0)
		player.pflags = $&~(PF_STARTJUMP|PF_SPINNING)
	end
end)

//Super sparkles

freeslot("SPR_SUSK",
	     "S_SUPERSPARK",
	     "MT_SUPERSPARKLES")

states[S_SUPERSPARK] = {
       sprite = SPR_SUSK,
	   frame = FF_FULLBRIGHT|FF_ANIMATE|A,
	   var1 = 4,
	   var2 = 3
}
mobjinfo[MT_SUPERSPARKLES] = {
         spawnstate = S_SUPERSPARK,
         flags = MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY|MF_NOCLIPHEIGHT,
}

local blacklist = {
	["dirk"] = true,
	["altsonic"] = true,
	["pointy"] = true,
	["fluffy"] = true,
	["mcsonic"]= true,
	["sms"] = true
}

sfxinfo[freeslot("sfx_mrfly")].caption = "Flight"

rawset(_G, "SkipBadnikScrap", {
	[MT_EGGANIMUS_EX] = 800,
})

--Triggers an event if you're SA-Sonic.

local function YuHyperMemories(line, so)
	if so and so.valid and so.player and so.player.yusonictable and not so.player.yusonictable.hyperpower and so.skin == "adventuresonic" and so.health
		S_StartSound(so, sfx_s3k7d)
		S_StartSound(so, sfx_s3kcel)
		so.player.yusonictable.hypermemories = 220
		so.player.yusonictable.superflash = 9*FRACUNIT
		so.player.yusonictable.hyperpower = true
		return false
	end
end
addHook("LinedefExecute", YuHyperMemories, "YUMEMO")

local bossesInfo = {
	{MT_EGGANIMUS, "Egg Animus", "MASTER OF LIGHT"},
	{MT_EGGANIMUS_EX, "Egg Animus EX", "MASTER OF LIGHT"},
	{MT_EGGBALLER, "Egg FireBaller", "BLAZING N BALLIN"},
	{MT_EGGEBOMBER, "Egg E-Bomber", "EXPLOSIVE SHOCKER"},
	{MT_EGGFREEZER, "Egg Freezer", "FROZEN NIGHTMARE"},
	{MT_FBOSS, "Egg Fighter", "SPARKING DEFENDER"},
	{MT_FBOSS2, "Egg Fighter", "SPARKING DEFENDER"},
	{MT_XBOSS, "Egg Mobile X", "ARMED SUPER WEAPON"}
}
if yakuzaBossTexts then
	for _,val in ipairs(bossesInfo) do
		if not (val[3]) then
			val[3] = ""
		end
		yakuzaBossTexts[val[1]] = {name = val[2], info = val[3]}
	end
end