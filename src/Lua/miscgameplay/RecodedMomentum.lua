--custom momentum made with a lot less hacks then CBWMom, heavily referenced from ChrispyChars. 
--Originally was gonna launch this in v2.0, but that's taking forever so i'm putting it in this minor update.
--ported from xmom v1.3 with assistance from frostiikin to suit the needs of MRCE
addHook("PreThinkFrame", function(p)
	for p in players.iterate
		if p.mrce.customskin > 0 --Set player.mrcecustomskin to 2 every frame on your character's end to qualify as a custom skin!
			p.mrce.customskin = $-1
			p.mrce.physics = false
			return
		end
		if p.mo.skin == "adventuresonic" or p.mrce.physics == false
			return
		end
		if (p.noxmomchar or p.xmomtoggledoff)
		return 
		end
		if p.spectator return false end
		if not p.realmo return false end
		if p.xmlastz == nil
			p.xmlastspeed = p.speed
			p.xmlastz = p.mo.z
			p.xmlastx = p.mo.x
			p.xmlasty = p.mo.y
			p.xmlastmomx = p.mo.momx
			p.xmomlastmomy = p.mo.momy
			p.xmlaststate = p.mo.state
		end
		if p.fakenormalspeed == nil
			p.fakenormalspeed = skins[p.mo.skin].normalspeed
		end
		local watermul = 1
		if (p.mo.eflags & MFE_UNDERWATER)
			watermul = 2
		end
		if (p.mo and p.mo.valid and p.mo.health)
			if p.xmlastskin and p.xmlastskin != p.mo.skin
				p.hasnomomentum = false
			end
			p.xmlastskin = p.mo.skin
		end
		local speed = FixedDiv(FixedHypot(p.mo.momx - p.cmomx, p.mo.momy - p.cmomy), p.mo.scale)
		local SPEED_INCREASE_LEEWAY = 0*FRACUNIT // the amount of speed above normalspeed needed to update normalspeed
		local SPEED_DECREASE_LEEWAY = 15*FRACUNIT // the amount of speed below normalspeed needed to update normalspeed
			if not p.hasnomomentum and not (p.pflags & PF_SPINNING)
				if p.restoremymomentumafteruncurlingpleasethankyouuuuu
					p.normalspeed = max(p.restoremymomentumafteruncurlingpleasethankyouuuuu, p.fakenormalspeed)
					p.restoremymomentumafteruncurlingpleasethankyouuuuu = nil
				end
				if p.xmlastz*P_MobjFlip(p.mo) > p.mo.z*P_MobjFlip(p.mo) and P_IsObjectOnGround(p.mo) and not (p.mo.eflags & MFE_JUSTHITFLOOR)
					--P_Thrust(p.mo, R_PointToAngle2(p.mo.x, p.mo.y, p.xmlastx, p.xmlasty)+FixedAngle(180*FRACUNIT), max(FRACUNIT*2+FRACUNIT/2, min((p.mo.z-p.xmlastz)/4*-1, 7*p.mo.scale)))
					p.normalspeed = $ + (p.mo.z*P_MobjFlip(p.mo)-p.xmlastz*P_MobjFlip(p.mo))/25*-1
					--print("aAAAAA")
				elseif p.powers[pw_super] and (p.mrce.canhyper or p.mrce.ultrastar) and mrce_hyperunlocked
					if P_IsObjectOnGround(p.mo)
						p.normalspeed = $ + FRACUNIT/16
					end
				else
					--if P_IsObjectOnGround(p.mo)
						--p.normalspeed = $ + FRACUNIT/15
					--end
				end
				--temporarily restoring how metal used to work in 1.2 
				--since I wouldn't want to make a major change to gameplay in a minor update
				local restorefakenormalspeed = p.fakenormalspeed
				if p.dashmode > 3*TICRATE
					p.fakenormalspeed = p.normalspeed
				end
				if not p.powers[pw_super] and not p.powers[pw_sneakers]
					if (speed*watermul > p.normalspeed + SPEED_INCREASE_LEEWAY
					or speed*watermul < p.normalspeed - SPEED_DECREASE_LEEWAY)
						p.normalspeed = max(speed*watermul, p.fakenormalspeed)
					end
				else
					if (speed*3/5*watermul > p.normalspeed + SPEED_INCREASE_LEEWAY
					or speed*3/5*watermul < p.normalspeed - SPEED_DECREASE_LEEWAY)
						--if not (p.powers[pw_sneakers])
						--if not (p.hyper and p.hyper.capable and p.powers[pw_super])
							p.normalspeed = max((speed*3/5)*watermul, p.fakenormalspeed)
						--else
							--p.normalspeed = max(min(speed*watermul, 160*FRACUNIT*watermul), p.fakenormalspeed)
						--end
							
					end
				end 
				p.fakenormalspeed = restorefakenormalspeed
				if p.normalspeed > 95*FRACUNIT*watermul --max speed cap
					p.normalspeed = $ - p.normalspeed/50
				end
			end
			local momangle = R_PointToAngle2(0,0,p.rmomx,p.rmomy) //Used for angling new momentum in ability cases
			local pmom = FixedDiv(FixedHypot(p.rmomx,p.rmomy),p.mo.scale) //Current speed, scaled for normalspeed calculations
			//Knuckles momentum renewal
			if p.charability == CA_GLIDEANDCLIMB then
				//Create glide history
				if p.glidelast == nil then
					p.glidelast = 0
				end
				local gliding = p.pflags&PF_GLIDING
				local thokked = p.pflags&PF_THOKKED
				local exitglide = (p.glidelast == 1 and not(gliding) and thokked)
				local landglide = (p.glidelast == 2 and not(gliding|thokked))
				//Restore glide momentum after deactivation
				if exitglide or landglide then
					p.mo.momx = p.xmlastmomx
					p.mo.momy = p.xmlastmomy
				end
				//Update glide history
				if gliding then
					p.glidelast = 1 //Gliding
				elseif exitglide then
					p.glidelast = 2 //Falling from glide
				elseif not(gliding|thokked) then
					p.glidelast = 0 //Not in glide state
				end
			end
				
			//////
			//Fang momentum renewal
			if p.charability == CA_BOUNCE then
				//Create bounce history
				if p.bouncelast == nil then
					p.bouncelast = false
				end
				if p.pflags&PF_BOUNCING and not(p.bouncelast) //Activate bounce
					or (not(p.pflags&PF_BOUNCING) and p.pflags&PF_THOKKED and p.bouncelast) //Deactivate bounce
					//Undo the momentum cut from bounce activation/deactivation
					p.mo.momx = p.xmlastmomx
					p.mo.momy = p.xmlastmomy
					p.mo.momz = $*2
				end
				//Update bounce history
				p.bouncelast = (p.pflags&PF_BOUNCING > 0)
			end
		p.xmlastspeed = p.speed
		p.xmlastz = p.mo.z
		p.xmlastx = p.mo.x
		p.xmlasty = p.mo.y
		p.xmlastmomx = p.mo.momx
		p.xmlastmomy = p.mo.momy
		p.xmlaststate = p.mo.state
		--local speed = FixedDiv(FixedHypot(p.mo.momx - p.cmomx, p.mo.momy - p.cmomy), p.mo.scale)
	end
end)