freeslot("MT_ICICLE", "sfx_s1dfal")

sfxinfo[sfx_s1dfal] = {
	singular = false,
	caption = "Falling icicle"
}

function A_SpawnIcicle(actor)
	if actor and actor.valid
		if leveltime % (2*TICRATE + (AngleFixed(actor.angle)/FRACUNIT)) == 0
			local spike = P_SpawnMobj(actor.x, actor.y, actor.z, MT_ICICLE)
			spike.scale = FRACUNIT * 2
			spike.shadowscale = spike.scale
		end
	end
end

addHook("MobjThinker", function(mobj)
	if mobj and mobj.valid
		if P_IsObjectOnGround(mobj)
			for i = 0, 8
				local fa = (i*(ANGLE_180/4))
				local shatter = P_SpawnMobj(mobj.x, mobj.y, mobj.z+FRACUNIT, MT_ROCKCRUMBLE9)
				shatter.momx = FixedMul(sin(fa),5*FRACUNIT)
				shatter.momy = FixedMul(cos(fa),5*FRACUNIT)
				P_SetObjectMomZ(shatter, 5*FRACUNIT, false)
				shatter.scale = FRACUNIT
				shatter.renderflags = $ | RF_FULLBRIGHT
			end
			S_StartSound(mobj, sfx_shattr)
			P_RemoveMobj(mobj)
		end
	end
end, MT_ICICLE)