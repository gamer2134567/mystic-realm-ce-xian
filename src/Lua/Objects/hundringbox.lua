freeslot("MT_HUNDRING_BOX")
local hasring = false
local soundcounter = 0
local killbox = false
function A_HundRingBox(actor)
	--repurposing this as the Ultra Ring, in singleplayer if you don't have infinite lives, it will give you a continue, otherwise it will give 100 rings. It will despawn in record attack
	//this is primarily because it is currently impossible to acquire continues in mrce stages without tokens
	if not (actor.target and actor.target.player)
		return
	end
	
	local player = actor.target.player
	if not netgame and player.lives != INFLIVES
		hasring = true
		S_StartSound(player.mo, sfx_s245)
	else
		P_GivePlayerRings(player, 100)
		S_StartSound(player.mo, sfx_kc33)
	end
end

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid return end
	
	if netgame return end

    if modeattacking or hasring == true
	and mobj.state != S_BOX_POP2
		P_RemoveMobj(mobj)
	end
end, MT_HUNDRING_BOX)

addHook("IntermissionThinker", function()
	local p = consoleplayer
	if netgame return end
	if modeattacking return end
	if not hasring return end
	soundcounter = $ + 1
	if soundcounter == 70 and p.continues <= 99 then
		S_StartSound(nil, sfx_s23f)
		p.continues = $ + 1
		hasring = false
		soundcounter = 0
	end
end)
