//this will force exit level to MAPB0 when the Egg Animus is defeated. If you intend to use the egg animus outside of mrce, make sure to edit line 14 to suit your needs
//the second phase in the soc with the rest of the boss. Or you could adapt this script for your own use, I don't care.
//The purpose of this script is to skip the score tally screen and move straight to Dimension Warp when the boss is defeated.

freeslot("MT_EGGANIMUS_EX", "MT_EGGANIMUS") // Freeslotting so I can reference it without breaking anything, since the rest of the enemy is defined in SOC.
local disruption = 7 * TICRATE
addHook("BossThinker", function(mo)
if not (mo.valid) return end
	if mo.type == MT_EGGANIMUS_EX and mo.health <= 0
		disruption = $ - 1
		if disruption <= 0
			disruption = 10 * TICRATE
			G_SetCustomExitVars(164,1)
			G_ExitLevel()
		end
	end
end)

--transform sound s3k46
--final blast s3k4e
--crack sound rail1
--quake sound rumble
--p.viewrollangle = P_RandomRange(1,359)*ANG1
--P_DoSuperTransformation(player, true)

addHook("ThinkFrame", do
	if mapheaderinfo[gamemap].lvlttl != "Mystic Realm" then return end
	for p in players.iterate()
		if p.riftbreak == nil then
			p.riftbreak = 0
		end
		if disruption < 5*TICRATE and disruption > 0
			p.viewrollangle = p.riftbreak
		end
		if disruption == 6 * TICRATE then
			S_StartSound(nil, sfx_rumble)
			P_StartQuake(70*FRACUNIT, 110)
		end
		if disruption == 3 * TICRATE then
			S_StartSound(nil, sfx_rumble)
			S_StartSound(nil,  sfx_rail1)
			p.riftbreak = P_RandomRange(-70,70)*ANG1
			P_FlashPal(p, 5, 15)
		end
		if disruption == 2 * TICRATE then
			S_StartSound(nil,  sfx_rail1)
			p.riftbreak = P_RandomRange(-70,70)*ANG1
			P_FlashPal(p, 5, 15)
		end
		if disruption == 1 * TICRATE then
			S_StartSound(nil,  sfx_rail1)
			p.riftbreak = P_RandomRange(-70,70)*ANG1
			P_FlashPal(p, 5, 15)
		end
		if disruption == 16 and p.mo.skin != "adventuresonic" and p.mo.skin != "sms" then
			S_StartSound(p.mo,  sfx_s3k46)
			P_DoSuperTransformation(p, true)
		end
		if disruption == 10 then
			S_StartSound(nil,  sfx_s3k4e)
		end
	end
end)