/*
	Saving Functions

	Stores all of the I/O functions for saving and loading in MRCE
	Used for the custom save select. Built to the same functionality
	as vanilla save select down to separated files for save slots.

	Emblems are stored in the save file so you can recollect them without
	having to reset your entire gamedata. Unlockables are still unlocked
	globally the first time you hit the emblem goal.

	(C) 2021-2022 by Ashi
*/

local saveinfo = {}

rawset(_G, "dat_saveinfo", saveinfo)

local function DAT_LoadSaveFile(saveslot)
	local file = io.openlocal("client/MRCE/save"..tostring(saveslot)..".dat", "r")

	if dat_saveinfo[saveslot] == nil then
		dat_saveinfo[saveslot] = {}
	end

	if file == nil then
		-- This slot doesn't contain save info. make the slot data read nil.
		dat_saveinfo[saveslot] = nil
	else
		local save = file:read("*a")
		if save == "NODATA" then
			-- We aren't given a proper method of deleting files.
			-- So we just overwrite it with NODATA instead
			dat_saveinfo[saveslot] = nil
		else
			dat_saveinfo[saveslot].skinname = save:match('skinname%s*=%s*"?(%a+)"?')
			dat_saveinfo[saveslot].mapnumber = save:match('mapnumber%s*=%s*(%d+)')
			dat_saveinfo[saveslot].lives = save:match('lives%s*=%s*(%d+)')
			dat_saveinfo[saveslot].continues = save:match('continues%s*=%s*(%d+)')
			dat_saveinfo[saveslot].score = save:match('score%s*=%s*(%d+)')
			dat_saveinfo[saveslot].emeralds = save:match('emeralds%s*=%s*(%d+)')
			dat_saveinfo[saveslot].episode = save:match('episode%s*=%s*"?(%a+)"?')
			dat_saveinfo[saveslot].gamecomplete = save:match('episodecompleted%s*=%s*"?(%a+)"?')
		end
	end
end

/*
	Deleting

	By far the easiest part.
	We literally just open a file
	and replace everything.
*/
local function DAT_DeleteSaveFile(saveslot)
	local file = io.openlocal("client/MRCE/save"..tostring(saveslot)..".dat", "w+")

	file:write("NODATA")
	file:close()
end

/*
	Saving

	not nearly as painful to create as
	loading was.
*/
local function DAT_SaveToFile(saveslot, buffdata)
	-- open our savedata for writing
	local file = io.openlocal("client/MRCE/save"..tostring(saveslot)..".dat", "w+")
	local buffer = buffdata -- this has all of our data for the string

	-- Save all of our data into the file
	file:write("[slot"..tostring(saveslot)..
		'] = {\n    skinname = "'..buffer.skinname..
		'",\n    mapnumber = '..tostring(buffer.mapnumber)..
		',\n    lives = '..tostring(buffer.xtralife)..
		',\n    continues = '..tostring(buffer.continues)..
		',\n    score = '..tostring(buffer.score)..
		',\n    emeralds = '..tostring(buffer.emeralds)..
		',\n    episode = '..buffer.episode..
		',\n    episodecompleted = '..buffer.episodecompleted..
		',\n}')
	file:close()
end

// Define all of our commands and functions
rawset(_G, "DAT_SaveToFile", DAT_SaveToFile)
rawset(_G, "DAT_LoadSaveFile", DAT_LoadSaveFile)
rawset(_G, "DAT_DeleteSaveFile", DAT_DeleteSaveFile)

COM_AddCommand("load_io_test", function(player /*unused*/, saveslot)
	DAT_LoadSaveFile(saveslot)
	COM_BufInsertText(consoleplayer, "map "..tostring(IntToExtMapNum(tonumber(dat_saveinfo[saveslot].mapnumber))).." -f")
	dat_saveinfo.loading = true
	dat_saveinfo.slotsel = saveslot
end)

COM_AddCommand("save_io_test", function(player, saveslot)
	local buffdata = {}
	buffdata.skinname = player.mo.skin
	buffdata.mapnumber = gamemap
	buffdata.xtralife = player.lives
	buffdata.continues = player.continues
	buffdata.score = player.score
	buffdata.emeralds = emeralds
	buffdata.episode = "mainquest"
	buffdata.episodecompleted = "false"

	DAT_SaveToFile(saveslot, buffdata)
end)

addHook("PlayerSpawn", function(player)
	if dat_saveinfo.loading == true then
		R_SetPlayerSkin(player, dat_saveinfo[dat_saveinfo.slotsel].skinname)
		player.score = tonumber(dat_saveinfo[dat_saveinfo.slotsel].score)
		player.lives = tonumber(dat_saveinfo[dat_saveinfo.slotsel].lives)
		player.continues = tonumber(dat_saveinfo[dat_saveinfo.slotsel].continues)

		emeralds = tonumber(dat_saveinfo[dat_saveinfo.slotsel].emeralds)
		dat_saveinfo.loading = false
	end
end)

print("\
===========================================\
         Custom Save/Unlock System\
             Created for MRCE\
            (C) 2021-2022 Ashi\
===========================================\
")