/*
MRCE Lua HUD

by ashi
*/

local xpos = -52
local slidein = "no"
local hticker = 0

local function DrawMRCEHUD(v, p, cam, ticker, endticker)
	if gamemap == 1001 then return end -- Adding this for the sprite model viewer I used while debugging.
	if p.spectator then return false end
	if not p.realmo then return false end
	if hticker == 58 then
		if xpos <= 16 then
			xpos = $ + 6
		end
	else
		hticker = $ + 2
	end
	if (p.realmo) and not (p.mo.skin == "modernsonic") and not (p.mo.skin == "duke") and not (srb2p) and not (maptol & TOL_NIGHTS) and not (G_IsSpecialStage(gamemap)) and gamemap != 99 then
		v.draw(xpos, 10, v.cachePatch("MRHSCORE"), V_SNAPTOLEFT|V_SNAPTOTOP, v.getColormap(p.realmo.skin, p.realmo.color))
		v.draw(xpos, 26, v.cachePatch("MRHTIME"), V_SNAPTOLEFT|V_SNAPTOTOP, v.getColormap(p.realmo.skin, p.realmo.color))
		if not ultimatemode
		or gamemap == 136 then
			v.draw(xpos, 42, v.cachePatch("MRHRINGS"), V_SNAPTOLEFT|V_SNAPTOTOP, v.getColormap(p.realmo.skin, p.realmo.color))
			DrawMotdString(v, 75*FRACUNIT, 44*FRACUNIT, FRACUNIT, tostring(p.rings), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
		end
		DrawMotdString(v, 75*FRACUNIT, 9*FRACUNIT, FRACUNIT, tostring(p.score), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
		if G_TicsToMinutes(p.realtime) < 10 then
			DrawMotdString(v, 75*FRACUNIT, 27*FRACUNIT, FRACUNIT, "0", "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
			DrawMotdString(v, 83*FRACUNIT, 27*FRACUNIT, FRACUNIT, G_TicsToMinutes(p.realtime), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
		else
			DrawMotdString(v, 75*FRACUNIT, 27*FRACUNIT, FRACUNIT, G_TicsToMinutes(p.realtime), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
		end
		if G_TicsToSeconds(p.realtime) < 10 then
			DrawMotdString(v, 99*FRACUNIT, 27*FRACUNIT, FRACUNIT, "0", "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
			DrawMotdString(v, 107*FRACUNIT, 27*FRACUNIT, FRACUNIT, G_TicsToSeconds(p.realtime), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS, 1)
		else
			DrawMotdString(v, 99*FRACUNIT, 27*FRACUNIT, FRACUNIT, G_TicsToSeconds(p.realtime), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS, 1)
		end
		if G_TicsToCentiseconds(p.realtime) < 10 then
			DrawMotdString(v, 123*FRACUNIT, 27*FRACUNIT, FRACUNIT, "0", "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
			DrawMotdString(v, 131*FRACUNIT, 27*FRACUNIT, FRACUNIT, G_TicsToCentiseconds(p.realtime), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS, 1)
		else
			DrawMotdString(v, 123*FRACUNIT, 27*FRACUNIT, FRACUNIT, G_TicsToCentiseconds(p.realtime), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS, 1)
		end
		v.draw(91, 27, v.cachePatch("MRCEFNTS"), V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
		v.draw(115, 27, v.cachePatch("MRCEFNTP"), V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
		if (p.rings == 0 and (leveltime/5%2) and not ultimatemode) or (gamemap == 136 and p.rings <= 10 and (leveltime/5%2)) then
			v.draw(xpos, 42, v.cachePatch("MRHRRING"), V_SNAPTOLEFT|V_SNAPTOTOP, v.getColormap(p.realmo.skin, p.realmo.color))
		end
		hud.disable("rings")
		hud.disable("time")
		hud.disable("score")
	end
end

addHook("MapLoad", function()
	xpos = -52
end)

hud.add(DrawMRCEHUD, "game")