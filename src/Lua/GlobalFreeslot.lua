-- placed here so it doesn't use it's own file lol
sfxinfo[sfx_kc5c].caption = "Shrine Activated"

--Credit to Motd for this
--Credit to Lach for the OG
-- alignment: -1 = right-aligned, 1 = left-aligned
rawset(_G, "DrawMotdString",
-- Displays a string with a custom font
---@param v drawer
---@param x integer
---@param y integer
---@param scale integer
---@param text string
---@param font string
---@param flags integer
---@param alignment integer
---@param color colormap
function(v, x, y, scale, text, font, flags, alignment, color)
	local right
	local colormap = v.getColormap(0, color)
	local start
	local finish
	alignment = $ or 1
	color = $ or 0
	right = alignment < 0
	text = tostring(text)

	if right then
		start = text:len()
    	finish = 1
	else
		start = 1
		finish = text:len()
	end

	for i = start, finish, alignment do
		local letter = font .. text:sub(i, i)
		if not v.patchExists(letter) then continue end

		local patch = v.cachePatch(letter)

		if right then -- right aligned, change offset before drawing
			x = $ - patch.width*scale
		end

		v.drawScaled(x, y, scale, patch, flags, colormap)

		if not right then -- left aligned, change offset after drawing
			x = $ + patch.width*scale
		end
	end
end)

rawset(_G, "IntToExtMapNum",
-- Borrowed from MapVote.lua
--IntToExtMapNum(n)
--Returns the extended map number as a string
--Returns nil if n is an invalid map number
---@param n number
function(n)
	if n < 0 or n > 1035 then
		return nil
	end
	if n < 10 then
		return "MAP0" + n
	end
	if n < 100 then
		return "MAP" + n
	end
	local x = n-100
	local p = x/36
	local q = x - (36*p)
	local a = string.char(65 + p)
	local b
	if q < 10 then
		b = q
	else
		b = string.char(55 + q)
	end
	return "MAP" + a + b
end)

rawset(_G, "GetNumberList",
--From CobaltBW. Thank you my dude!
--This is used to turn a level header variable into a table
--via detecting a "," as a separator.
---@param str string
function(str)
	local t = {}
	while str do
		local sep = string.find(str,'%,')
		if sep != nil then
			local arg = string.sub(str,0,sep-1)
			local tag = tonumber(arg)
			if tag then
				table.insert(t, tag)
			elseif tag != 0 then
				print('Invalid argument '..arg)
				break
			end
			str = string.sub($,sep+1)
		else
			local tag = tonumber(str)
			if tag then
				table.insert(t, tag)
			end
			break
		end
	end
	return t
end)

-- Misc variables used throughout multiple scripts
rawset(_G, "pi", 22*FRACUNIT/7) -- Used in multiple objects and bosses
rawset(_G, "dispstaticlogo", false) -- Credits

rawset(_G, "debugmenu", false) -- Used to hide titlescreen elements for debug menus

-- Menu Table (Contains every single menu that the game can pull up)
-- Each of these entries get a value set on menu init
-- That value being the actual menu table for each menu
rawset(_G, "menutable", {
	-- Main Menu
	"mainmenu",
	"multiplayer", -- Links to OG SRB2 menu
	"extras",
	"addons", -- Links to OG SRB2 menu
	"quit",

	-- Singleplayer Menu
	"singleplayer",
		------Save select------
			"saveselect",
			"characterselect", -- Save creation
			"episodeselect",
		------Save select------
	"recordattack",
	"marathonmode",
	"statistics",

	-- Options
	"options",
	"controloptions", -- Links to OG SRB2 menu
	"videooptions",
	"soundoptions", -- Links to OG SRB2 menu
	"dataoptions",
	
	-- Advanced Options
	"advancedoptions",
	"fulloptions" -- Links to OG Options menu
})

rawset(_G, "ctrl_inputs", {
	-- movement
    up = {}, down = {}, left = {}, right = {},
    -- main
    jmp = {}, spn = {}, cb1 = {}, cb2 = {}, cb3 = {},
    -- sys
    sys = {}, pause = {}, con = {}
})

-- fill out these on map load
addHook("MapLoad", function()
    ctrl_inputs.up[1], ctrl_inputs.up[2] = input.gameControlToKeyNum(GC_FORWARD)
	ctrl_inputs.down[1], ctrl_inputs.down[2] = input.gameControlToKeyNum(GC_BACKWARD)
	ctrl_inputs.left[1], ctrl_inputs.left[2] = input.gameControlToKeyNum(GC_STRAFELEFT)
	ctrl_inputs.right[1], ctrl_inputs.right[2] = input.gameControlToKeyNum(GC_STRAFERIGHT)

	ctrl_inputs.jmp[1], ctrl_inputs.jmp[2] = input.gameControlToKeyNum(GC_JUMP)
	ctrl_inputs.spn[1], ctrl_inputs.spn[2] = input.gameControlToKeyNum(GC_SPIN)
	ctrl_inputs.cb1[1], ctrl_inputs.cb1[2] = input.gameControlToKeyNum(GC_CUSTOM1)
    ctrl_inputs.cb2[1], ctrl_inputs.cb2[2] = input.gameControlToKeyNum(GC_CUSTOM2)
    ctrl_inputs.cb3[1], ctrl_inputs.cb3[2] = input.gameControlToKeyNum(GC_CUSTOM3)

	ctrl_inputs.sys[1], ctrl_inputs.sys[2] = input.gameControlToKeyNum(GC_SYSTEMMENU)
	ctrl_inputs.pause[1], ctrl_inputs.pause[2] = input.gameControlToKeyNum(GC_PAUSE)
    ctrl_inputs.con[1], ctrl_inputs.con[2] = input.gameControlToKeyNum(GC_CONSOLE)
end)
