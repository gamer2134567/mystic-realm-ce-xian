addHook("PreThinkFrame",do
    for p in players.iterate
		if not p.realmo return false end
		if p.spectator return false end
		if p.mrce == nil
			local mrce = {
			glowaura = false,
			flycheat = false,
			hypercheat = false,
			canhyper = false,
			ultrastar = false,
			hyperimages = false,
			hypermode = false,
			customskin = 0,
			dontwantphysics = false,
			physics = true,
			skipmystic = false,
			nasyamystic = false,
			exspark = false,
			ishyper = false,
			jump = 0
			}
			if p.mo
				p.mrce = mrce
			end
		end
	end
end)

if not (yakuzaBossTexts) then
	rawset(_G, "yakuzaBossTexts", {})
end

addHook("PlayerThink", function(p)
	if p.spectator return false end
	if not p.realmo return false end
	
	if p.cmd.buttons & BT_JUMP then
		p.mrce.jump = $ + 1
	else
		p.mrce.jump = 0
	end
end)
